<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function(){
    session([
        'ip'=>getHostByName(getHostName())
    ]);
    if(Auth::check()){
        return redirect('/employees');
    }else{
        return redirect('/login');
    }
});

Route::group(['middleware'=>['guest', 'revalidate']], function(){
	Route::post('/validate_login', ['as'=>'validate_login', 'uses'=>'Auth\LoginController@validate_login']);

	Route::post('/login', ['as'=>'login', 'uses'=>'Auth\LoginController@login']);
});

Route::group(['middleware'=>['auth', 'revalidate']], function(){
	Route::resource('employees', 'EmployeesController');

	Route::resource('departments', 'DepartmentController');

	Route::resource('audit-trail', 'AuditController');

	Route::resource('sites', 'SitesController');

	Route::resource('change-password', 'ChangePasswordController');

	Route::get('/autocomplete', 'DepartmentController@autocomplete');

	Route::get('/autocomplete/user', 'EmployeesController@autocomplete');

	Route::get('/autocomplete/site', 'SitesController@autocomplete');

	Route::get('/datatable/employeeData', 'DatatablesController@employeeData');

	Route::get('/datatable/departmentData', 'DatatablesController@departmentData');

	Route::get('/datatable/auditTrailData', 'DatatablesController@auditTrailData');

	Route::get('/datatable/siteData', 'DatatablesController@siteData');

	Route::get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'FileController@importExportExcelORCSV'));

	Route::post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'FileController@importFileIntoDB'));

	Route::get('storage/{filename}', 'FileController@downloadExcelFile');

	Route::post('batchUpdate',array('as'=>'batchUpdate','uses'=>'FileController@batchUpdate'));
});

Route::get('/admin/clear-cache', function(){
	$exitCode = Artisan::call('cache:clear');
	return redirect('/');
});