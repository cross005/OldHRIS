@extends('adminlte::page')

@section('content_header')
<h1>Employees</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<div class="row">
	<div class="col-sm-2">
		<select id="filterOption" class="form-control">
			<option value="all">All</option>
			<option value="full_name">Full Name</option>
			<option value="date_hired">Date Hired</option>
			<option value="department">Department</option>
			<option value="designation">Designation</option>
		</select>
	</div> 
	<div class="col-sm-3">
		<input type="text" name="search" id="search" class="form-control" placeholder="Enter keyword here">

		<div class="input-group" id="datePicker">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right" id="date_hired" type="text">
		</div>

	</div>	
</div><br>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>id</th>
			<th>ID Number</th>
			<th>Full Name</th>
			<th>Birth Date</th>
			<th>Age</th>
			<th>Birth Place</th>
			<th>Gender</th>
			<th>Civil Status</th>
			<th>Religion</th>
			<th>Citizenship</th>
			<th>Date Hired</th>
			<th>Employment Status</th>
			<th>Regularization Date</th>
			<th>Separation Date</th>
			<th>Department</th>
			<th>Designation</th>
			<th>Level</th>
			<th>Site</th>
			<th>Tax Status</th>
			<th>SSS</th>
			<th>TIN</th>
			<th>HDMF</th>
			<th>PHIC</th>
			<th>BPI Account Number</th>
			<th>Salary</th>
			<th>Company E-Mail</th>
			<th>Present Address</th>
			<th>Personal Contact Number</th>
			<th>Work Contact Number</th>
			<th>Contact Person Name</th>
			<th>Contact Person relationship</th>
			<th>Contact Person Address</th>
			<th>Contact Person contact number</th>
			<th>School</th>
			<th>Degree</th>
			<th>Professional License</th>
			<th>License No.</th>
			<th>Action</th>
		</tr>
	</thead>
</table>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Import Employees</h4>
			</div>
			<div class="modal-body">
				<form id="importEmployee" action="{{ route('import-csv-excel') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group {{ $errors->has('employeeRecords') ? 'has-error':'' }}">
						<div class="col-sm-12">
							<label>Select File to import</label>
							<input type="file" name="employeeRecords">
							<p class="help-block">Accepted file types: csv, xls, xlsx</p>
							@if($errors->has('employeeRecords'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('employeeRecords') }}</span>
							@endif

							<a href="/storage/template.xlsx" class="btn btn-default">Download Template</a>

							@isset($fieldErrors)
							<div class="well">
								Errors:<br>
								@for($i = 0; $i < count($fieldErrors); $i++)
								{{ 'Row: '.$fieldErrors[$i]['row'] . ' - ' . $fieldErrors[$i]['error_message'] }} <br>
								@endfor
							</div>
							<a href="/storage/{{ $fieldErrors[0]['file_name'] }}.xlsx" class="btn btn-danger" id="exportErrors">Export Errors</a>
							@endisset
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" name="submit" id="submitBtn" value="Upload">
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="batchUpdateModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Update Employees</h4>
			</div>
			<div class="modal-body">
				<form id="importEmployee" action="{{ route('batchUpdate') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="form-group {{ $errors->has('employeeRecords') ? 'has-error':'' }}">
						<div class="col-sm-12">
							<label>Select File to import</label>
							<input type="file" name="employeeRecords">
							<p class="help-block">Accepted file types: csv, xls, xlsx</p>
							@if($errors->has('employeeRecords'))
							<span id="helpBlock2" class="help-block">{{ $errors->first('employeeRecords') }}</span>
							@endif

							<a href="/storage/template.xlsx" class="btn btn-default">Download Template</a>

							@isset($fieldErrors)
							<div class="well">
								Errors:<br>
								@for($i = 0; $i < count($fieldErrors); $i++)
								{{ 'Row: '.$fieldErrors[$i]['row'] . ' - ' . $fieldErrors[$i]['error_message'] }} <br>
								@endfor
							</div>
							<a href="/storage/{{ $fieldErrors[0]['file_name'] }}.xlsx" class="btn btn-danger" id="exportErrors">Export Errors</a>
							@endisset
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" name="submit" id="submitBtn" value="Upload">
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop

@section('js')
@isset($employeeUpdated)
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Employee Profile has been updated." }
		}).show();
	});
</script>
@endisset

@isset($allEmployeeSuccess)
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "All records inserted without an error." }
		}).show();
	});
</script>
@endisset

@isset($allEmployeeUpdateSuccess)
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "All records updated without an error." }
		}).show();
	});
</script>
@endisset

<script>
	@if($errors->has('employeeRecords') || isset($fieldErrors))
	$('#uploadModal').modal('show')
	@endif

	$("#importEmployee").on('submit', function(){
		$("#submitBtn").html("<i class='fa fa-spinner fa-spin'></i>");
		$("#submitBtn").attr('disabled', 'disabled');
	})

	$('#datePicker').addClass('hidden');

	$('#date_hired').daterangepicker();

	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/employeeData',
		columns: [
		{ data: 'id', visible: false },
		{ data: 'employee_id', visible: false },
		{ data: 'full_name', width: '30%' },
		{ data: 'birth_date', visible: false },
		{ data: null, visible: false },
		{ data: 'birth_place', visible: false },
		{ data: 'gender', visible: false },
		{ data: 'civil_status', visible: false },
		{ data: 'religion', visible: false },
		{ data: 'citizenship', visible: false },
		{ data: 'date_hired', width: '10%' },
		{ data: 'employee_status', visible: false },
		{ data: 'regularization_date', visible: false },
		{ data: 'separation_date', visible: false },
		{ data: 'name', width: '20%' },
		{ data: 'designation', width: '20%' },
		{ data: 'level', visible: false },
		{ data: 'site', visible: false },
		{ data: 'tax_status', visible: false },
		{ data: 'sss', visible: false },
		{ data: 'tin', visible: false },
		{ data: 'hdmf', visible: false },
		{ data: 'phic', visible: false },
		{ data: 'bpi', visible: false },
		{ data: 'salary', visible: false },
		{ data: 'email', visible: false },
		{ data: 'present_address', visible: false },
		{ data: 'mobile_number', visible: false },
		{ data: 'work_mobile_number', visible: false },
		{ data: 'cp_name', visible: false },
		{ data: 'cp_relationship', visible: false },
		{ data: 'cp_address', visible: false },
		{ data: 'cp_mobile', visible: false },
		{ data: 'school', visible: false },
		{ data: 'degree', visible: false },
		{ data: 'professional_license', visible: false },
		{ data: 'license_number', visible: false },
		{ data: null, width: '100%', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"l>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/employees/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/employees/"+data.id+"/edit' class='btn btn-default'>Update</a>";

				return btn;
			}
		},
		{
			targets: [4],
			render: function(data, type, row){
				var today = new Date();
				var birthDate = new Date(data.birth_date);
				var age = today.getFullYear() - birthDate.getFullYear();
				var m = today.getMonth() - birthDate.getMonth();
				if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) 
				{
					age--;
				}
				return age;
			}
		}
		],
		buttons: [
		{
			text: 'Batch Update',
			action: function ( e, dt, node, config ) {
				$('#batchUpdateModal').modal('show')
			}
		},
		{
			text: 'Import',
			action: function ( e, dt, node, config ) {
				$('#uploadModal').modal('show')
			}
		},
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34]
			}
		},
		{
			text: 'Create Employee Profile',
			action: function ( e, dt, node, config ) {
				window.location.href="/employees/create";
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});

	$('#search').on( 'keyup', function () {
		var col;
		switch($('#filterOption').val()){
			case 'full_name':
			col = 2;
			break;
			case 'date_hired':
			col = 10;
			break;
			case 'department':
			col = 14;
			break;
			case 'designation':
			col = 15;
			break;
			default:
			col = 5;
			break;
		}
		if(col == 5){
			table
			.search( this.value )
			.draw();
		}else{
			table
			.columns( col )
			.search( this.value )
			.draw();
		}
	} );

	var dateFrom, dateTo;

	$('#date_hired').on('apply.daterangepicker', function(ev, picker) {
		dateFrom = new Date($(this).val().substring(0,10));
		dateTo = new Date($(this).val().substring(13,23));
		$.fn.dataTable.ext.search.push(
			function( settings, data, dataIndex ) {
				var dateHired = new Date(data[10]);
				if(dateHired >= dateFrom && dateHired <= dateTo){
					return true;
				}
				return false;
			}
			);
		table.draw();
	});

	$("#filterOption").on('change', function(e){
		e.preventDefault();
		table
		.search( '' )
		.columns().search( '' )
		.draw();
		if($(this).val() == 'date_hired'){
			$('#datePicker').removeClass('hidden');
			$('#search').addClass('hidden');
		}else{
			$('#search').removeClass('hidden');
			$('#datePicker').addClass('hidden');
		}

		$('#search').val('');
	})
</script>
@stop