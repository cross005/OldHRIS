@extends('adminlte::page')

@section('content_header')
<h1>@if(isset($showEmployee)) 
	<a href='/employees' title='Go Back' class='unstyle' style="font-size: 1em!important;"><i class='fa fa-arrow-left'></i></a> 
	@endif 
	@isset($showEmployee)
	{{ ucfirst($showEmployee->firstname) . '\'s Employee Profile' }}
	@endisset

	@isset($updateEmployee)
	{{ 'Update ' . ucfirst($updateEmployee->firstname) . '\'s Employee Profile' }}
	@endisset

	@if(!isset($showEmployee) && !isset($updateEmployee))
	{{ 'Create employee profile' }}
	@endif
</h1>
@if(!isset($showEmployee) && !isset($updateEmployee))
<h6 style="color: red;"><i>* - required fields</i></h6>
@endif
@stop

@section('content')
<div class='notifications top-right'></div>
<form class="form-horizontal" action="{{ isset($updateEmployee) ? action('EmployeesController@update', $updateEmployee->id) : action('EmployeesController@store') }}" method="POST" id="createEmployeeForm">
	@if(!isset($showEmployee))
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	@endif
	@isset($updateEmployee)
	<input type="hidden" name="_method" value="patch" />
	@endisset

	<div class="box box-default box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Personal Information</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('employee_id') ? 'has-error':'' }}">
					<label for="employee_id" class="col-sm-3 control-label">ID Number *</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="employee_id" value="{{ old('employee_id') && !isset($updateEmployee) ? old('employee_id') : '' }}{{ isset($showEmployee->employee_id) ? $showEmployee->employee_id : '' }}{{ isset($updateEmployee->employee_id) ? $updateEmployee->employee_id : '' }}" {{ isset($showEmployee->employee_id) ? 'readonly' : '' }}>
						@if($errors->has('employee_id'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('employee_id') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('birth_date') ? 'has-error':'' }}">
					<label for="birth_date" class="col-sm-3 control-label">Birth Date *</label>
					<div class="col-sm-9">
						<input type="{{ isset($showEmployee) ? 'text' : 'date' }}" class="form-control" name="birth_date" value="{{ old('birth_date') && !isset($updateEmployee) ? old('birth_date') : '' }}{{ isset($showEmployee->birth_date) ? $showEmployee->birth_date : '' }}{{ isset($updateEmployee->birth_date) ? $updateEmployee->birth_date : '' }}" {{ isset($showEmployee->birth_date) ? 'readonly' : '' }}>
						@if($errors->has('birth_date'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('birth_date') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('firstname') ? 'has-error':'' }}">
					<label for="firstname" class="col-sm-3 control-label">First Name *</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="firstname" value="{{ old('firstname') && !isset($updateEmployee) ? old('firstname') : '' }}{{ isset($showEmployee->firstname) ? $showEmployee->firstname : '' }}{{ isset($updateEmployee->firstname) ? $updateEmployee->firstname : '' }}" {{ isset($showEmployee->firstname) ? 'readonly' : '' }}>
						@if($errors->has('firstname'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('firstname') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('nameExtension') ? 'has-error':'' }}">
					<label for="nameExtension" class="col-sm-3 control-label">Name Extension</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="nameExtension" value="{{ old('nameExtension') && !isset($updateEmployee) ? old('nameExtension') : '' }}{{ isset($showEmployee->name_extension) ? $showEmployee->name_extension : '' }}{{ isset($updateEmployee->name_extension) ? $updateEmployee->name_extension : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('nameExtension'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('nameExtension') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('middlename') ? 'has-error':'' }}">
					<label for="middlename" class="col-sm-3 control-label">Middle Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="middlename" value="{{ old('middlename') && !isset($updateEmployee) ? old('middlename') : '' }}{{ isset($showEmployee->middlename) ? $showEmployee->middlename : '' }}{{ isset($updateEmployee->middlename) ? $updateEmployee->middlename : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('middlename'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('middlename') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('lastname') ? 'has-error':'' }}">
					<label for="lastname" class="col-sm-3 control-label">Last Name *</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="lastname" value="{{ old('lastname') && !isset($updateEmployee) ? old('lastname') : '' }}{{ isset($showEmployee->lastname) ? $showEmployee->lastname : '' }}{{ isset($updateEmployee->lastname) ? $updateEmployee->lastname : '' }}" {{ isset($showEmployee->lastname) ? 'readonly' : '' }}>
						@if($errors->has('lastname'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('lastname') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('birth_place') ? 'has-error':'' }}">
					<label for="birth_place" class="col-sm-3 control-label">Birth Place</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="birth_place" value="{{ old('birth_place') && !isset($updateEmployee) ? old('birth_place') : '' }}{{ isset($showEmployee->birth_place) ? $showEmployee->birth_place : '' }}{{ isset($updateEmployee->birth_place) ? $updateEmployee->birth_place : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('birth_place'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('birth_place') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('gender') ? 'has-error':'' }}">
					<label for="gender" class="col-sm-3 control-label">Gender *</label>
					<div class="col-sm-9">
						<div class="form-group">
							<div class="col-sm-6">
								<label>
									<input name="gender" id="male" value="male" {{ isset($showEmployee) ? 'disabled=""' : '' }} {{ isset($showEmployee) && strtolower($showEmployee->gender) == 'male' ? 'checked=""' : '' }}{{ isset($updateEmployee) && strtolower($updateEmployee->gender) == 'male' ? 'checked=""' : '' }}{{ old('gender') && old('gender') == 'male' ? 'checked=""' : '' }} type="radio">
									Male
								</label>
							</div>
							<div class="col-sm-6">
								<label>
									<input name="gender" id="female" {{ isset($showEmployee) ? 'disabled=""' : '' }} value="female" {{ isset($showEmployee) && strtolower($showEmployee->gender) == 'female' ? 'checked=""' : '' }}{{ isset($updateEmployee) && strtolower($updateEmployee->gender) == 'female' ? 'checked=""' : '' }}{{ old('gender') && old('gender') == 'female' ? 'checked=""' : '' }} type="radio">
									Female
								</label>
							</div>
						</div>
						@if($errors->has('gender'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('gender') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('civilStatus') ? 'has-error':'' }}">
					<label for="civil_status" class="col-sm-3 control-label">Civil Status *</label>
					<div class="col-sm-9">
						{{-- <input type="text" class="form-control" name="civil_status" value="{{ old('civil_status') && !isset($updateEmployee) ? old('civil_status') : '' }}{{ isset($showEmployee->civil_status) ? $showEmployee->civil_status : '' }}{{ isset($updateEmployee->civil_status) ? $updateEmployee->civil_status : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}> --}}
						@if(isset($showEmployee))
						<input type="text" class="form-control" name="civil_status" value="{{ isset($showEmployee->civil_status) ? $showEmployee->civil_status : '' }}" {{ isset($showEmployee->civil_status) ? 'readonly' : '' }}>
						@else
						<select id="civilStatus" name="civilStatus" class="form-control">
							<option></option>
							<option value="Single">Single</option>
							<option value="Married">Married</option>
							<option value="Widowed">Widowed</option>
							<option value="Legally Separated">Legally Separated</option>
						</select>
						@endif
						@if($errors->has('civilStatus'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('civilStatus') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('religion') ? 'has-error':'' }}">
					<label for="religion" class="col-sm-3 control-label">Religion</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="religion" value="{{ old('religion') && !isset($updateEmployee) ? old('religion') : '' }}{{ isset($showEmployee->religion) ? $showEmployee->religion : '' }}{{ isset($updateEmployee->religion) ? $updateEmployee->religion : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('religion'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('religion') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('citizenship') ? 'has-error':'' }}">
					<label for="citizenship" class="col-sm-3 control-label">Citizenship</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="citizenship" value="{{ old('citizenship') && !isset($updateEmployee) ? old('citizenship') : '' }}{{ isset($showEmployee->citizenship) ? $showEmployee->citizenship : '' }}{{ isset($updateEmployee->citizenship) ? $updateEmployee->citizenship : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('citizenship'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('citizenship') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Employment Information</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('date_hired') ? 'has-error':'' }}">
					<label for="date_hired" class="col-sm-3 control-label">Date Hired *</label>
					<div class="col-sm-9">
						<input type="{{ isset($showEmployee) ? 'text' : 'date' }}" class="form-control" name="date_hired" value="{{ old('date_hired') && !isset($updateEmployee) ? old('date_hired') : '' }}{{ isset($showEmployee->date_hired) ? $showEmployee->date_hired : '' }}{{ isset($updateEmployee->date_hired) ? $updateEmployee->date_hired : '' }}" {{ isset($showEmployee->date_hired) ? 'readonly' : '' }}>
						@if($errors->has('date_hired'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('date_hired') }}</span>
						@endif
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('department') ? 'has-error':'' }}">
					<label for="department" class="col-sm-3 control-label">Department *</label>
					<div class="col-sm-9">
						@if(isset($showEmployee))
						<input type="text" class="form-control" name="department" value="{{ isset($showEmployee->deptName) ? $showEmployee->deptName : '' }}" {{ isset($showEmployee->deptName) ? 'readonly' : '' }}>
						@else
						<select id="departmentName" class="form-control" name="department" {{ isset($showEmployee) ? 'readonly' : '' }}></select>
						@endif
						@if($errors->has('department'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('department') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('position') ? 'has-error':'' }}">
					<label for="position" class="col-sm-3 control-label">Designation *</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="position" value="{{ old('position') && !isset($updateEmployee) ? old('position') : '' }}{{ isset($showEmployee->position) ? $showEmployee->position : '' }}{{ isset($updateEmployee->position) ? $updateEmployee->position : '' }}" {{ isset($showEmployee->position) ? 'readonly' : '' }}>
						@if($errors->has('position'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('position') }}</span>
						@endif
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('employee_status') ? 'has-error':'' }}">
					<label for="employee_status" class="col-sm-3 control-label">Employee Status *</label>
					<div class="col-sm-9">
						@if(isset($showEmployee))
						<input type="text" class="form-control" name="firstname" value="{{ isset($showEmployee->deptName) ? $showEmployee->employee_status : '' }}" {{ isset($showEmployee->employee_status) ? 'readonly' : '' }}>
						@else
						<select class="form-control" name="employee_status">
							<option></option>
							<option value="Probationary">Probationary</option>
							<option value="Regular">Regular</option>
							<option value="Re-hire">Re-hire</option>
							<option value="Consultant">Consultant</option>
							<option value="Project Based">Project Based</option>
							<option value="Intern">Intern</option>
							<option value="Resigned">Resigned</option>
						</select>
						@endif
					</div>
					@if($errors->has('employee_status'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('employee_status') }}</span>
					@endif
					@if($errors->has('duplicateName'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('duplicateName') }}</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('regularization_date') ? 'has-error':'' }}">
					<label for="regularization_date" class="col-sm-3 control-label">Regularization Date</label>
					<div class="col-sm-9">
						<input type="{{ isset($showEmployee) ? 'text' : 'date' }}" class="form-control" name="regularization_date" value="{{ old('regularization_date') && !isset($updateEmployee) ? old('regularization_date') : '' }}{{ isset($showEmployee->regularization_date) ? $showEmployee->regularization_date : '' }}{{ isset($updateEmployee->regularization_date) ? $updateEmployee->regularization_date : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('regularization_date'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('regularization_date') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('separation_date') ? 'has-error':'' }}">
					<label for="separation_date" class="col-sm-3 control-label">Separation Date</label>
					<div class="col-sm-9">
						<input type="{{ isset($showEmployee) ? 'text' : 'date' }}" class="form-control" name="separation_date" value="{{ old('separation_date') && !isset($updateEmployee) ? old('separation_date') : '' }}{{ isset($showEmployee->separation_date) ? $showEmployee->separation_date : '' }}{{ isset($updateEmployee) ? $updateEmployee->separation_date : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('separation_date'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('separation_date') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('site') ? 'has-error':'' }}">
					<label for="site" class="col-sm-3 control-label">Site *</label>
					<div class="col-sm-9">
						@if(isset($showEmployee))
						<input type="text" class="form-control" name="site" value="{{ isset($showEmployee->site) ? $showEmployee->site : '' }}" {{ isset($showEmployee->site) ? 'readonly' : '' }}>
						@else
						<select id="site" class="form-control" name="site" {{ isset($showEmployee) ? 'readonly' : '' }}></select>
						@endif
						@if($errors->has('site'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('site') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('tax_status') ? 'has-error':'' }}">
					<label for="tax_status" class="col-sm-3 control-label">Tax Status</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="tax_status" value="{{ old('tax_status') && !isset($updateEmployee) ? old('tax_status') : '' }}{{ isset($showEmployee->tax_status) ? $showEmployee->tax_status : '' }}{{ isset($updateEmployee->tax_status) ? $updateEmployee->tax_status : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('tax_status'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('tax_status') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('sss') ? 'has-error':'' }}">
					<label for="sss" class="col-sm-3 control-label">SSS</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="sss" value="{{ old('sss') && !isset($updateEmployee) ? old('sss') : '' }}{{ isset($showEmployee->sss) ? $showEmployee->sss : '' }}{{ isset($updateEmployee->sss) ? $updateEmployee->sss : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('sss'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('sss') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('tin') ? 'has-error':'' }}">
					<label for="tin" class="col-sm-3 control-label">TIN</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="tin" value="{{ old('tin') && !isset($updateEmployee) ? old('tin') : '' }}{{ isset($showEmployee->tin) ? $showEmployee->tin : '' }}{{ isset($updateEmployee->tin) ? $updateEmployee->tin : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('tin'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('tin') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('hdmf') ? 'has-error':'' }}">
					<label for="hdmf" class="col-sm-3 control-label">HDMF</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="hdmf" value="{{ old('hdmf') && !isset($updateEmployee) ? old('hdmf') : '' }}{{ isset($showEmployee->hdmf) ? $showEmployee->hdmf : '' }}{{ isset($updateEmployee->hdmf) ? $updateEmployee->hdmf : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('hdmf'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('hdmf') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('phic') ? 'has-error':'' }}">
					<label for="phic" class="col-sm-3 control-label">PHIC</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="phic" value="{{ old('phic') && !isset($updateEmployee) ? old('phic') : '' }}{{ isset($showEmployee->phic) ? $showEmployee->phic : '' }}{{ isset($updateEmployee->tax_status) ? $updateEmployee->phic : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('phic'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('phic') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('bpi') ? 'has-error':'' }}">
					<label for="bpi" class="col-sm-3 control-label">BPI Account Number</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="bpi" value="{{ old('bpi') && !isset($updateEmployee) ? old('bpi') : '' }}{{ isset($showEmployee->bpi) ? $showEmployee->bpi : '' }}{{ isset($updateEmployee->bpi) ? $updateEmployee->bpi : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('bpi'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('bpi') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('salary') ? 'has-error':'' }}">
					<label for="salary" class="col-sm-3 control-label">Salary</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="salary" value="{{ old('salary') && !isset($updateEmployee) ? old('salary') : '' }}{{ isset($showEmployee->salary) ? $showEmployee->salary : '' }}{{ isset($updateEmployee->tax_status) ? $updateEmployee->salary : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('salary'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('salary') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('email') ? 'has-error':'' }}">
					<label for="email" class="col-sm-3 control-label">Company E-Mail *</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="email" value="{{ old('email') ? old('email') : '' }}{{ isset($showEmployee->company_email) ? $showEmployee->company_email : '' }}{{ isset($updateEmployee->company_email) && !old('email') ? $updateEmployee->company_email : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('email'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('email') }}</span>
						@endif
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('level') ? 'has-error':'' }}">
					<label for="level" class="col-sm-3 control-label">Level *</label>
					<div class="col-sm-9">
						@if(isset($showEmployee))
						<input type="text" class="form-control" name="level" value="{{ isset($showEmployee->level) ? $showEmployee->level : '' }}" {{ isset($showEmployee->level) ? 'readonly' : '' }}>
						@else
						<select id="level" name="level" class="form-control">
							<option></option>
							<option>Associate</option>
							<option>Senior Associate</option>
							<option>Team Lead</option>
							<option>Officer</option>
							<option>Manager</option>
							<option>Head</option>
						</select>
						@endif
						@if($errors->has('level'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('level') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Contact Info</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('present_address') ? 'has-error':'' }}">
					<label for="present_address" class="col-sm-3 control-label">Present Address</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="present_address" value="{{ old('present_address') && isset($updateEmployee) ? old('present_address') : '' }}{{ old('present_address') && !isset($updateEmployee) ? old('present_address') : '' }}{{ isset($showEmployee->present_address) ? $showEmployee->present_address : '' }}{{ isset($updateEmployee->present_address) ? $updateEmployee->present_address : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('present_address'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('present_address') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('mobile_number') ? 'has-error':'' }}">
					<label for="mobile_number" class="col-sm-3 control-label">Mobile Number</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="mobile_number" value="{{ old('mobile_number') && isset($updateEmployee) ? old('mobile_number') : '' }}{{ old('mobile_number') && !isset($updateEmployee) ? old('mobile_number') : '' }}{{ isset($showEmployee->mobile_number) ? $showEmployee->mobile_number : '' }}{{ isset($updateEmployee->mobile_number) ? $updateEmployee->mobile_number : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('mobile_number'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('mobile_number') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('personal_email') ? 'has-error':'' }}">
					<label for="personal_email" class="col-sm-3 control-label">Personal Email</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="personal_email" value="{{ old('personal_email') && isset($updateEmployee) ? old('personal_email') : '' }}{{ old('personal_email') && !isset($updateEmployee) ? old('personal_email') : '' }}{{ isset($showEmployee->personal_email) ? $showEmployee->personal_email : '' }}{{ isset($updateEmployee->personal_email) ? $updateEmployee->personal_email : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('personal_email'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('personal_email') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Emergency Contact Person</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('cp_name') ? 'has-error':'' }}">
					<label for="cp_name" class="col-sm-3 control-label">Name</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="cp_name" value="{{ old('cp_name') && isset($updateEmployee) ? old('cp_name') : '' }}{{ old('cp_name') && !isset($updateEmployee) ? old('cp_name') : '' }}{{ isset($showEmployee->cp_name) ? $showEmployee->cp_name : '' }}{{ isset($updateEmployee->cp_name) ? $updateEmployee->cp_name : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('cp_name'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('cp_name') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('cp_relationship') ? 'has-error':'' }}">
					<label for="cp_relationship" class="col-sm-3 control-label">Relationship</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="cp_relationship" value="{{ old('cp_relationship') && isset($updateEmployee) ? old('cp_relationship') : '' }}{{ old('cp_relationship') && !isset($updateEmployee) ? old('cp_relationship') : '' }}{{ isset($showEmployee->cp_relationship) ? $showEmployee->cp_relationship : '' }}{{ isset($updateEmployee->cp_relationship) ? $updateEmployee->cp_relationship : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('cp_relationship'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('cp_relationship') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('cp_address') ? 'has-error':'' }}">
					<label for="cp_address" class="col-sm-3 control-label">Address</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="cp_address" value="{{ old('cp_address') && isset($updateEmployee) ? old('cp_address') : '' }}{{ old('cp_address') && !isset($updateEmployee) ? old('cp_address') : '' }}{{ isset($showEmployee->cp_address) ? $showEmployee->cp_address : '' }}{{ isset($updateEmployee->cp_address) ? $updateEmployee->cp_address : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('cp_address'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('cp_address') }}</span>
						@endif
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('cp_mobile') ? 'has-error':'' }}">
					<label for="cp_mobile" class="col-sm-3 control-label">Contact Number</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="cp_mobile" value="{{ old('cp_mobile') && isset($updateEmployee) ? old('cp_mobile') : '' }}{{ old('cp_mobile') && !isset($updateEmployee) ? old('cp_mobile') : '' }}{{ isset($showEmployee->cp_mobile) ? $showEmployee->cp_mobile : '' }}{{ isset($updateEmployee->cp_mobile) ? $updateEmployee->cp_mobile : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('cp_mobile'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('cp_mobile') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>

	<div class="box box-default box-solid">
		<div class="box-header with-border">
			<h3 class="box-title">Educational Information</h3>

			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="">
			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('school') ? 'has-error':'' }}">
					<label for="school" class="col-sm-3 control-label">School</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="school" value="{{ old('school') && isset($updateEmployee) ? old('school') : '' }}{{ old('school') && !isset($updateEmployee) ? old('school') : '' }}{{ isset($showEmployee->school) ? $showEmployee->school : '' }}{{ isset($updateEmployee->school) ? $updateEmployee->school : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('school'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('school') }}</span>
						@endif
					</div>
				</div>
				<div class="col-sm-6 form-group {{ $errors->has('degree') ? 'has-error':'' }}">
					<label for="degree" class="col-sm-3 control-label">Degree</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="degree" value="{{ old('degree') && isset($updateEmployee) ? old('degree') : '' }}{{ old('degree') && !isset($updateEmployee) ? old('degree') : '' }}{{ isset($showEmployee->degree) ? $showEmployee->degree : '' }}{{ isset($updateEmployee->degree) ? $updateEmployee->degree : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('degree'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('degree') }}</span>
						@endif
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group {{ $errors->has('professional_license') ? 'has-error':'' }}">
					<label for="professional_license" class="col-sm-3 control-label">Professional License</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="professional_license" value="{{ old('professional_license') && isset($updateEmployee) ? old('professional_license') : '' }}{{ old('professional_license') && !isset($updateEmployee) ? old('professional_license') : '' }}{{ isset($showEmployee->professional_license) ? $showEmployee->professional_license : '' }}{{ isset($updateEmployee->professional_license) ? $updateEmployee->professional_license : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('professional_license'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('professional_license') }}</span>
						@endif
					</div>
				</div>

				<div class="col-sm-6 form-group {{ $errors->has('license_number') ? 'has-error':'' }}">
					<label for="license_number" class="col-sm-3 control-label">License Number</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="license_number" value="{{ old('license_number') && isset($updateEmployee) ? old('license_number') : '' }}{{ old('license_number') && !isset($updateEmployee) ? old('license_number') : '' }}{{ isset($showEmployee->license_number) ? $showEmployee->license_number : '' }}{{ isset($updateEmployee->license_number) ? $updateEmployee->license_number : '' }}" {{ isset($showEmployee) ? 'readonly' : '' }}>
						@if($errors->has('license_number'))
						<span id="helpBlock2" class="help-block">{{ $errors->first('license_number') }}</span>
						@endif
					</div>
				</div>
			</div>
		</div>
		<!-- /.box-body -->
	</div>
	<br>
	@if(!isset($showEmployee))
	<div class="row">
		<input type="submit" id="hiddenSubmit" value="submit" name="submit" hidden>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="col-sm-4">
				<input type="button" name="save" data-loading-text="Loading..." id="submitBtn" value="Save" class="form-control btn btn-default">
			</div>

			@if(!isset($updateEmployee))
			<div class="col-sm-4">
				<input type="button" name="clear" id="clearBtn" value="Clear" class="form-control btn btn-default">
			</div>
			@endif

			<div class="col-sm-4">
				<input type="button" name="cancel" id="cancelBtn" value="Cancel" class="form-control btn btn-default">
			</div>
		</div>
	</div>
	@endif
</form>
@stop

@section('js')
@if(isset($employeeCreated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Employee Profile has been created." }
		}).show();
	});
</script>
@endif

<script>
	@if(isset($showEmployee))
	$('#departmentName').val('{{ $showEmployee->deptName }}').trigger('change');
	$('select[name="employee_status"]').val('{{ $showEmployee->employee_status }}').trigger('change');
	$('select[name="site"]').val('{{ $showEmployee->site }}').trigger('change');
	@endif

	@isset($updateEmployee)
	var option = new Option('{{ $updateEmployee->deptName }}', '{{ $updateEmployee->department_id }}', true, true);
	var option2 = new Option('{{ $updateEmployee->employee_status }}', '{{ $updateEmployee->employee_status }}', true, true);
	var option3 = new Option('{{ $updateEmployee->civil_status }}', '{{ $updateEmployee->civil_status }}', true, true);
	var option4 = new Option('{{ $updateEmployee->site }}', '{{ $updateEmployee->site }}', true, true);
	var option5 = new Option('{{ $updateEmployee->level }}', '{{ $updateEmployee->level }}', true, true);
	$('#departmentName').append(option).trigger('change');
	$('select[name="employee_status"]').append(option2).trigger('change');
	$('select[name="civilStatus"]').append(option3).trigger('change');
	$('select[name="site"]').append(option4).trigger('change');
	$('select[name="level"]').append(option5).trigger('change');

	$('#departmentName').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateEmployee->deptName }}'
		}
	});

	$('select[name="employee_status"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateEmployee->employee_status }}'
		}
	});

	$('select[name="civilStatus"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateEmployee->civil_status }}'
		}
	});

	$('select[name="site"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateEmployee->site }}'
		}
	});

	$('select[name="level"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateEmployee->level }}'
		}
	});
	@endisset

	@if(old('level'))
	var option = new Option('{{ old('level') }}', '{{ old('level') }}', true, true);
	$('#level').append(option).trigger('change');
	$('select[name="level"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ old('level') }}'
		}
	});
	@endif

	@if(old('civilStatus'))
	var option = new Option('{{ old('civilStatus') }}', '{{ old('civilStatus') }}', true, true);
	$('#civilStatus').append(option).trigger('change');
	$('select[name="civilStatus"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ old('civilStatus') }}'
		}
	});
	@endif

	
	@if(old('employee_status'))
	var option = new Option('{{ old('employee_status') }}', '{{ old('employee_status') }}', true, true);
	$('select[name="employee_status"]').append(option).trigger('change');
	$('select[name="employee_status"]').trigger({
		type: 'select2:select',
		params: {
			data: '{{ old('employee_status') }}'
		}
	});
	@endif

	$(document).ready(function($) {
		$('select[name="employee_status"]').select2({
			allowClear: true
		});

		$('#level').select2({
			allowClear: true
		});

		$('#civilStatus').select2({
			allowClear: true
		});

		$('#departmentName').select2({
			allowClear: true,
			ajax: {
				url: '/autocomplete',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});

		$('#site').select2({
			allowClear: true,
			ajax: {
				url: '/autocomplete/site',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});
	})

	$("#submitBtn").on('click', function(e){
		e.preventDefault()
		swal({
			title: '',
			text: 'Are you sure you want to submit this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#hiddenSubmit').click();
		});
	})

	$('#clearBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to clear the form?',
			text: 'Doing so will reset the form',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#createEmployeeForm')[0].reset();
			$('select').val(null).trigger('change');
		});
	})

	$('#cancelBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to leave this page?',
			text: 'This will reset all your inputs',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/employees";
		});
	})

	$("#createEmployeeForm").on('submit', function(e){
		$("#submitBtn").button('loading');
	})
</script>
@stop