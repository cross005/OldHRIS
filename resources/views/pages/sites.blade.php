@extends('adminlte::page')

@section('content_header')
<h1>Sites</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>id</th>
			<th>Site Code</th>
			<th>Site Name</th>
			<th>Action</th>
		</tr>
	</thead>
</table>
@stop

@section('js')

@if(isset($siteUpdated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "{{ $siteUpdated }} has been updated." }
		}).show();
	});
</script>
@endif

<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/siteData',
		columns: [
		{ data: 'id', visible: false },
		{ data: 'site_code', width: '50%' },
		{ data: 'site_name', width: '100%' },
		{ data: null, width: '250px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/sites/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/sites/"+data.id+"/edit' class='btn btn-default'>Update</a>";

				return btn;
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [1,2]
			}
		},
		{
			text: 'Create Site',
			action: function ( e, dt, node, config ) {
				window.location.href="/sites/create";
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
</script>
@stop