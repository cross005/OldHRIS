@extends('adminlte::page')

@section('content_header')
<h1>Departments</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>id</th>
			<th>Department</th>
			<th>Supervisor</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
	</thead>
</table>
@stop

@section('js')

@if(isset($departmentUpdated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "{{ $departmentUpdated }} Department has been updated." }
		}).show();
	});
</script>
@endif

<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/departmentData',
		columns: [
		{ data: 'id', visible: false },
		{ data: 'name', width: '50%' },
		{ data: 'supervisor', width: '100%' },
		{ data: 'status', width: '100%' },
		{ data: null, width: '250px', searchable: false, sortable: false}
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		columnDefs: [
		{
			targets: -1,
			render: function(a, b, data, d){
				var btn = "<a href='/departments/"+ data.id +"' class='btn btn-default'>View</a>";
				btn += "<a href='/departments/"+data.id+"/edit' class='btn btn-default'>Update</a>";

				if(data.status == 'active'){
					btn += "<a href='' data-type='deactivate' data-id='"+data.id+"' class='btn btn-default indAction'>Deactivate</a>";
				}else{
					btn += "<a href='' data-type='activate' data-id='"+data.id+"' class='btn btn-default indAction'>Activate</a>";
				}

				return btn;
			}
		}
		],
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [1,2,3,4]
			}
		},
		{
			text: 'Create Department',
			action: function ( e, dt, node, config ) {
				window.location.href="/departments/create";
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$(document).on('click', '.indAction', function(e){
		e.preventDefault();
		var type = $(this).data('type');
		var id = $(this).data('id');
		swal({
			title: '',
			text: 'Are you sure you want to '+ type +' this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$.ajax({
				url: '/departments/'+id,
				type: 'PATCH',
				data: {id: id, type: type},
				success: function(data, status){
					if(data == 'success'){
						swal('Success', 'The department has been '+type+'d.', 'success').then(function(){
							table.ajax.reload();
						});
					}else{
						swal('Error', 'There was an error in processing your request.', 'error').then(function(){
							table.ajax.reload();
						});
					}
				}
			});
		});
	})
</script>
@stop