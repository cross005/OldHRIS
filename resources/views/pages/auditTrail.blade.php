@extends('adminlte::page')

@section('content_header')
<h1>Audit Trail</h1>
@stop

@section('content')
@isset($test)
<table id="asd" class="table table-striped table-hover table-bordered text-center hidden">
	<thead>
		<tr>
			<th>Employee Name</th>
			@for($i = 0; $i < $test->systemCount; $i++)
			<th>{{ $test[$i]->name }}</th>
			@endfor
		</tr>
	</thead>
	<tbody>
		@for($i = 0; $i < $test->empCount; $i++)
		<tr>
			<td>{{ $test->empList[$i]->name }}</td>
			@for($i = 0; $i < $test->systemCount; $i++)
			<td><input type="checkbox" data-id="{{ $test[$i]->id }}" {{ $test[$i]->hasAccess == 1 ? 'checked=""' : '' }} name=""></td>
			@endfor
		</tr>
		@endfor
	</tbody>
</table>
@endisset

<div class='notifications top-right'></div>
<table id="mainTable" class="table table-striped table-hover table-bordered text-center">
	<thead>
		<tr>
			<th>id</th>
			<th>Employee ID</th>
			<th>Full Name</th>
			<th>Action</th>
			<th>Module</th>
			<th>Date and Time</th>
		</tr>
	</thead>
</table>
@stop

@section('js')
<script>
	var table = $('#mainTable').DataTable({
		processing: true,
		'order': [],
		ajax: '/datatable/auditTrailData',
		columns: [
		{ data: 'id', visible: false },
		{ data: 'employee_id', width: '50px' },
		{ data: 'full_name', width: '30%' },
		{ data: 'action', width: '100%' },
		{ data: 'module', width: '150px' },
		{ data: 'created_date', width: '100px' },
		],
		dom: 'r<"pull-right"B><"pull-left"lf>tip',
		lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
		pageLength: 10,
		buttons: [
		{
			extend: 'excel',
			text: 'Export',
			exportOptions: {
				columns: [1,2,3,4,5]
			}
		}
		],
		"scrollX": true,
		"fixedHeader": true
	});
</script>
@stop