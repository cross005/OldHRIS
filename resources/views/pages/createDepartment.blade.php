@extends('adminlte::page')

@section('content_header')
<h1>@if(isset($updateDepartment)) {{ 'Update '. $updateDepartment->name }} @elseif(isset($showDepartment)) <a href='/departments' title='Go Back' class='unstyle'><i class='fa fa-arrow-left'></i></a>  {{ 'View ' }}  @else {{ 'Create' }} @endisset Department</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<form class="form-horizontal" id="createDepartmentForm" action="{{ isset($updateDepartment) ? action('DepartmentController@update', $updateDepartment->id) : action('DepartmentController@store') }}" method="POST">
	@isset($updateDepartment)
	<input type="hidden" name="_method" value="patch" />
	@endisset
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
{{-- 	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('department_id') ? 'has-error':'' }}">
				<label for="department_id" class="col-sm-5">Department Code</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="department_id" placeholder="Enter department code here" value="{{ isset($showDepartment) ? $showDepartment->department_id : '' }}{{ isset($updateDepartment) ? $updateDepartment->department_id : '' }}" {{ isset($showDepartment) ? 'readonly' : '' }}>
					@if($errors->has('department_id'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('department_id') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div> --}}

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('deptName') ? 'has-error':'' }}">
				<label for="deptName" class="col-sm-5">Department Name</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="deptName" placeholder="Enter department name here" value="{{ isset($showDepartment) ? $showDepartment->name : '' }}{{ isset($updateDepartment) ? $updateDepartment->name : ''}}" {{ isset($showDepartment) ? 'readonly' : '' }}>
					@if($errors->has('deptName'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('deptName') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('supervisor') ? 'has-error':'' }}">
				<label for="supervisor" class="col-sm-5">Supervisor</label>
				@if(isset($showDepartment))
				<div class="col-sm-7">
					<input type="text" name="" class="form-control" value="{{ isset($showDepartment) ? $showDepartment->employee_name : '' }}" {{ isset($showDepartment) ? 'readonly' : '' }}>
				</div>
				@else
				<div class="col-sm-7">
					<select class="form-control" name="supervisor"></select>
					@if($errors->has('supervisor'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('supervisor') }}</span>
					@endif
				</div>
				@endif
			</div>
		</div>
	</div>

	@if(!isset($showDepartment))
	<div class="row">
		<input type="submit" id="hiddenSubmit" value="submit" name="submit" hidden>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="col-sm-4">
				<input type="button" name="save" id="submitBtn" value="Save" class="form-control btn btn-default">
			</div>

			@if(!isset($updateDepartment))
			<div class="col-sm-4">
				<input type="button" name="clear" id="clearBtn" value="Clear" class="form-control btn btn-default">
			</div>
			@endif

			<div class="col-sm-4">
				<input type="button" name="cancel" id="cancelBtn" value="Cancel" class="form-control btn btn-default">
			</div>
		</div>
	</div>
	@endif
</form>
@stop

@section('js')
@if(isset($departmentCreated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Department has been created." }
		}).show();
	});
</script>
@endif

<script>
	@isset($updateDepartment)
	var option = new Option('{{ $updateDepartment->supervisor_id . ' ' . $updateDepartment->supervisor_firstname . ' ' . $updateDepartment->supervisor_lastname }}', '{{ $updateDepartment->supervisor }}', true, true);
	$('select').append(option).trigger('change');

	$('select').trigger({
		type: 'select2:select',
		params: {
			data: '{{ $updateDepartment->supervisor_id }}'
		}
	});
	@endisset

	$(document).ready(function(){
		$('select').select2({
			placeholder: "Select supervisor here",
			allowClear: true,
			ajax: {
				url: '/autocomplete/user',
				dataType: 'json',
				data: function(params) {
					return {
						term: params.term
					}
				},
				processResults: function (data, page) {
					return {
						results: data
					};
				},
			}
		});
	})

	$("#submitBtn").on('click', function(e){
		e.preventDefault()
		swal({
			title: '',
			text: 'Are you sure you want to submit this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#hiddenSubmit').click();
		});
	})

	$('#clearBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to clear the form?',
			text: 'Doing so will reset the form',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#createDepartmentForm')[0].reset();
			$('select').val(null).trigger('change');
		});
	})

	$('#cancelBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to leave this page?',
			text: 'This will reset all your inputs',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/departments";
		});
	})
</script>
@stop