@extends('adminlte::page')

@section('content_header')
<h1>Change Password</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<form class="form-horizontal" id="changePasswordForm" action="{{ action('ChangePasswordController@update', session('employee_id')) }}" method="POST">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" name="_method" value="patch">

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('oldPassword') ? 'has-error':'' }}">
				<label for="oldPassword" class="col-sm-5">Old Password</label>
				<div class="col-sm-7">
					<input type="password" name="oldPassword" class="form-control">
					@if($errors->has('oldPassword'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('oldPassword') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('newPassword') ? 'has-error':'' }}">
				<label for="newPassword" class="col-sm-5">New Password</label>
				<div class="col-sm-7">
					<input type="password" name="newPassword" class="form-control">
					@if($errors->has('newPassword'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('newPassword') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('newPassword_confirmation') ? 'has-error':'' }}">
				<label for="newPassword_confirmation" class="col-sm-5">Confirm New Password</label>
				<div class="col-sm-7">
					<input type="password" name="newPassword_confirmation" class="form-control">
					@if($errors->has('newPassword_confirmation'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('newPassword_confirmation') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<input type="submit" id="hiddenSubmit" value="submit" name="submit" hidden>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="col-sm-4">
				<input type="button" name="save" id="submitBtn" value="Save" class="form-control btn btn-default">
			</div>

			<div class="col-sm-4">
				<input type="button" name="cancel" id="cancelBtn" value="Cancel" class="form-control btn btn-default">
			</div>
		</div>
	</div>
</form>
@stop

@section('js')
@if(isset($passwordUpdated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Password Updated." }
		}).show();
	});
</script>
@endif

@if(isset($invalidPass))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: 'Old password does not match.',
			type: 'danger'
		}).show();
	});
</script>
@endif

<script>
	$("#submitBtn").on('click', function(e){
		e.preventDefault()
		swal({
			title: '',
			text: 'Are you sure you want to submit this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#hiddenSubmit').click();
		});
	})

	$('#cancelBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to leave this page?',
			text: 'This will reset all your inputs',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/departments";
		});
	})
</script>
@stop