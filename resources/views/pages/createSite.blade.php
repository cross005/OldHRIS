@extends('adminlte::page')

@section('content_header')
<h1>@if(isset($updateSite)) {{ 'Update '. $updateSite->site_code }} @elseif(isset($showSite)) <a href='/sites' title='Go Back' class='unstyle'><i class='fa fa-arrow-left'></i></a>  {{ 'View ' }}  @else {{ 'Create' }} @endisset Site</h1>
@stop

@section('content')
<div class='notifications top-right'></div>
<form class="form-horizontal" id="createSiteForm" action="{{ isset($updateSite) ? action('SitesController@update', $updateSite->id) : action('SitesController@store') }}" method="POST">
	@isset($updateSite)
	<input type="hidden" name="_method" value="patch" />
	@endisset
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('site_code') ? 'has-error':'' }}">
				<label for="site_code" class="col-sm-5">Site Code</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="site_code" value="{{ isset($showSite) ? $showSite->site_code : '' }}{{ isset($updateSite) ? $updateSite->site_code : '' }}" {{ isset($showSite) ? 'readonly' : '' }}>
					@if($errors->has('site_code'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('site_code') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="form-group {{ $errors->has('site_name') ? 'has-error':'' }}">
				<label for="site_name" class="col-sm-5">Site Name</label>
				<div class="col-sm-7">
					<input type="text" class="form-control" name="site_name" value="{{ isset($showSite) ? $showSite->site_name : '' }}{{ isset($updateSite) ? $updateSite->site_name : ''}}" {{ isset($showSite) ? 'readonly' : '' }}>
					@if($errors->has('site_name'))
					<span id="helpBlock2" class="help-block">{{ $errors->first('site_name') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>

	@if(!isset($showSite))
	<div class="row">
		<input type="submit" id="hiddenSubmit" value="submit" name="submit" hidden>
		<div class="col-sm-6 col-sm-offset-3">
			<div class="col-sm-4">
				<input type="button" name="save" id="submitBtn" value="Save" class="form-control btn btn-default">
			</div>

			@if(!isset($updateSite))
			<div class="col-sm-4">
				<input type="button" name="clear" id="clearBtn" value="Clear" class="form-control btn btn-default">
			</div>
			@endif

			<div class="col-sm-4">
				<input type="button" name="cancel" id="cancelBtn" value="Cancel" class="form-control btn btn-default">
			</div>
		</div>
	</div>
	@endif
</form>
@stop

@section('js')
@if(isset($siteCreated))
<script>
	$(document).ready(function(){
		$('.top-right').notify({
			message: { text: "Site has been created." }
		}).show();
	});
</script>
@endif

<script>
	$("#submitBtn").on('click', function(e){
		e.preventDefault()
		swal({
			title: '',
			text: 'Are you sure you want to submit this request?',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#hiddenSubmit').click();
		});
	})

	$('#clearBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to clear the form?',
			text: 'Doing so will reset the form',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			$('#createSiteForm')[0].reset();
		});
	})

	$('#cancelBtn').on('click', function(e){
		e.preventDefault();
		swal({
			title: 'Are you sure you want to leave this page?',
			text: 'This will reset all your inputs',
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Yes',
			type: 'question'
		}).then(function(){
			window.location.href = "/sites";
		});
	})
</script>
@stop