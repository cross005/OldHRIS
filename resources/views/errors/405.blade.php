@extends('errors::layout')

@section('title', 'Method not allowed')

@section('message', 'Sorry, you are trying to access a page on an invalid method.')
