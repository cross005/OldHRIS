@component('mail::message')
# User Credentials

Thank you accessing IT Enterprise Application/s. To proceed, please login using the following credentials:

Email: {{ $content['email'] }}
Password: {{ $content['pass'] }}

Please note that you can modify your password via Forgot Password feature. Also, abovementioned credentials can be used to login across all systems developed by the IT Enterprise Application Team (e.i. Service Desk, HRIS, PRPO Automation, Material Requisition Tool).

@endcomponent
