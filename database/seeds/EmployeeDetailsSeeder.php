<?php

use Illuminate\Database\Seeder;

class EmployeeDetailsSeeder extends Seeder
{
/**
* Run the database seeds.
*
* @return void
*/
public function run()
{
    DB::table('employee_details')->insert([
        [
            'employee_id' => 550,
            'firstname'=>'Kristine',
            'lastname'=>'Dela Cruz',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'Business Analyst',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'female',
            'civil_status'=>'single',
            'site'=>'makati'
        ],

        [
            'employee_id' => 551,
            'firstname'=>'Anton',
            'lastname'=>'panghulan',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'Programmer',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'male',
            'civil_status'=>'single',
            'site'=>'makati'
        ],


        [
            'employee_id' => 552,
            'firstname'=>'Ivan Ralph',
            'lastname'=>'Vitto',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'Programmer',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'male',
            'civil_status'=>'single',
            'site'=>'makati'
        ],


        [
            'employee_id' => 553,
            'firstname'=>'Ivann',
            'lastname'=>'Turla',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'Network Engineer',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'male',
            'civil_status'=>'single',
            'site'=>'makati'
        ],

        [
            'employee_id' => 554,
            'firstname'=>'Jino',
            'lastname'=>'Marchadesch',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'Network Engineer',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'male',
            'civil_status'=>'single',
            'site'=>'makati'
        ],


        [
            'employee_id' => 555,
            'firstname'=>'Rein',
            'lastname'=>'Guillermo',
            'department_id'=>'IT',
            'date_hired'=>'2017-01-01',
            'position'=>'IT asset and compliance',
            'level'=>'Associate',
            'employee_status'=>'Regular',
            'birth_date'=>'2017-01-01',
            'gender'=>'female',
            'civil_status'=>'single',
            'site'=>'makati'
        ],
    ]);
}
}
