<?php

use Illuminate\Database\Seeder;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('site_details')->insert([
        	[
	            'site_code'=>'SPCRPI',
	            'site_name'=>'Solar Philippines Commercial Rooftop Projects Inc.'
	        ],
	        [
                'site_code'=>'SPMMC',
                'site_name'=>'Solar Philippines Module Manufacturing Corporation'
            ],
            [
	            'site_code'=>'SPCC',
	            'site_name'=>'Solar Philippines Calatagan Corporation'
	        ]
        ]);
    }
}
