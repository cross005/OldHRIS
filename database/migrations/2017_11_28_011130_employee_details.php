<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EmployeeDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('firstname', 20);
            $table->string('name_extension')->nullable();
            $table->string('middlename', 20)->nullable();
            $table->string('lastname', 20);
            $table->string('birth_place')->nullable();
            $table->date('birth_date');
            $table->string('gender');
            $table->string('civil_status');
            $table->string('religion')->nullable();
            $table->string('citizenship')->nullable();
            $table->string('department_id');
            $table->date('date_hired');
            $table->string('position');
            $table->string('level');
            $table->string('employee_status');
            $table->date('regularization_date')->nullable();
            $table->date('separation_date')->nullable();
            $table->string('site');
            $table->integer('salary')->nullable();
            $table->longText('present_address')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('personal_email')->nullable();
            $table->string('work_mobile_number')->nullable();
            $table->string('cp_name')->nullable();
            $table->string('cp_relationship')->nullable();
            $table->longText('cp_address')->nullable();
            $table->string('cp_mobile')->nullable();
            $table->string('school')->nullable();
            $table->string('degree')->nullable();
            $table->string('professional_license')->nullable();
            $table->string('license_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_details');
    }
}
