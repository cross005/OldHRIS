<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompensationBenefits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compensation_benefits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('tax_status')->nullable();
            $table->string('sss')->nullable();
            $table->string('tin')->nullable();
            $table->string('hdmf')->nullable();
            $table->string('phic')->nullable();
            $table->string('bpi')->nullable();
            $table->integer('salary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compensation_benefits');
    }
}
