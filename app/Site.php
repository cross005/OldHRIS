<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'site_details';
    public $primaryKey = 'id';

    public $timestamps = false;

    public $fillable = ['site_code', 'site_name'];
}
