<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Site;
use App\Audit;

class SitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.sites');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createSite');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'site_code'=>'required|unique:site_details,site_code',
            'site_name'=>'required|unique:site_details,site_name'
        ]);

        Site::create([
            'site_code'=>$request->input('site_code'),
            'site_name'=>$request->input('site_name')
        ]);

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Created ' . $request->input('site_code') . ' Site',
            'module'=>'Sites'
        ]);

        return view('pages.createSite')->with('siteCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $site = Site::where('id', $id)->firstOrFail();

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Viewed ' . $site->site_code . ' Site',
            'module'=>'Sites'
        ]);

        return view('pages.createSite')->with('showSite', $site);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $site = Site::where('id', $id)->firstOrFail();
        return view('pages.createSite')->with('updateSite', $site);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'site_code'=> [
                'required',
                Rule::unique('site_details')->ignore($id),
            ],
            'site_name'=>'required'
        ]);

        $site = Site::where('id', $id)->update([
            'site_code'=>$request->input('site_code'),
            'site_name'=>$request->input('site_name')
        ]);

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Updated ' . $request->input('site_code') . ' Site',
            'module'=>'Update Site'
        ]);

        return view('pages.sites')->with('siteUpdated', $request->input('site_code'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function autocomplete()
    {
        $data = Site::select('site_code', 'site_name')->get();

        $formatted_tags = [];

        foreach ($data as $tag) {
            $formatted_tags[] = ['id' => $tag->site_code, 'text' => $tag->site_name];
        }
        return response()->json($formatted_tags);
    }
}
