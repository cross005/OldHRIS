<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendPass;
use App\User;
use App\Employee;
use App\AccessMatrix;
use App\Department;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
{
        $this->middleware('guest')->except('logout');
    }

    public function validate_login(Request $request){
        $this->validate($request, [
            'email'=>'required|exists:mysql2.ltxx_systemadmin.users,email|email'
        ]);

        try{
            $user = User::where('email',$request->input('email'))->firstOrFail();
        }
        catch(ModelNotFoundException $err){
            return view('auth.login')->with('emailNotExist', true);
        }
        if($user->status != 'Active'){
            return view('auth.login')->with('userInactive', true);
        }

        $access = AccessMatrix::join('tjsg_hris.department_details AS dd', 'ltxx_systemadmin.access_matrix_department.department_id', '=', 'dd.department_id')
        ->join('tjsg_hris.employee_details as ed', 'dd.department_id', '=', 'ed.department_id')
        ->join('ltxx_systemadmin.system_details AS sd', 'access_matrix_department.system_id', '=', 'sd.id')
        ->where([
            'sd.system_code'=>'hris',
            'access_matrix_department.hasAccess'=>"1",
            'access_matrix_department.department_id'=>\DB::raw('ed.department_id'),
            'ed.employee_id'=>$user->employee_id
        ])->first();

        if(!$access){
            return view('auth.login')->with('noAccess', true);
        }

        $user = User::select('password')->where('email', $request->input('email'))->first();
        if($user->password == ""){
            $this->sendPassToEmail($request->input('email'));

            return view('auth.login')->with('noPass', true);
        }

        $request->flashOnly(['email']);
        return view('auth.login')->with('emailValidated', true);
    }

    public function login(Request $request){
        $this->validate($request, [
            'email'=>'required|exists:mysql2.ltxx_systemadmin.users,email|email',
            'password'=>'required'
        ]);

        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'status'=>'active'])){
            $userDetails = User::where('email', $request->input('email'))->first();
            $emp = Employee::where('employee_id', $userDetails->employee_id)->first();
            session([
                'user_id' => $userDetails->id,
                'employee_id'=>$userDetails->employee_id,
                'user_name'=>$emp->firstname. ' '. $emp->lastname
            ]);
            $userAuth = User::find(session('user_id'));
            Auth::login($userAuth);
            return redirect('/')->with('user', $userDetails);
        }else{
            $request->flashExcept('password');
            return view('auth.login')->with('invalidPass', true);
        }
    }

    public function sendPassToEmail($email){
        $num = rand(1, 1000);
        $hash = substr(md5($num), 0, 8);
        $content = [

            'title'=> 'Human Resources Information System', 

            'email'=>$email,

            'pass'=>$hash
            ];

        $user = User::where('email', $email)->update([
                'password'=>bcrypt($hash)
            ]);

        Mail::to($email)->send(new SendPass($content));
    }

    public function test(){
        $user = User::where('email', 'ralph.vitto@solarphilippines.ph')->update([
            'status'=>'Active'
        ]);
    }
}
