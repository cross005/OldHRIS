<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Department;
use App\Employee;
use App\Audit;
use App\AccessMatrix;
use DB;

class DepartmentController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('pages.departments');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('pages.createDepartment');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'deptName'=>'required|unique:department_details,name',
            'supervisor'=>'required|exists:employee_details,employee_id'
        ]);

        $deptId = Department::orderBy('id', 'DESC')->first();

        $code = explode(' ', $request->input('deptName'));
        $deptCode = "";
        foreach($code as $c){
            $deptCode .= ucwords(substr($c, 0, 1));
        }

        $pre = '';
        for($i = 4; $i > strlen($deptId->id); $i--){
            $pre .= '0';
        }

        $department = new Department;
        $department->department_id = date('Y') . $pre . $deptId->id + 1 . $deptCode;
        $department->name = $request->input('deptName');
        $department->supervisor = $request->input('supervisor');
        $department->save();

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Created ' . $department->name . ' Department',
            'module'=>'Departments'
        ]);

        $system = DB::table('ltxx_systemadmin.system_details')->select('id')->get();

        foreach ($system as $sys) {
            AccessMatrix::create([
                'department_id'=>date('Y') . $pre . $deptId->id + 1 . $deptCode,
                'system_id'=>$sys->id,
                'hasAccess'=>0
            ]);
        }

        return view('pages.createDepartment')->with('departmentCreated', true);
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $department = Department::where('id', $id)->firstOrFail();
        $employee = Employee::where('employee_id', $department->supervisor)->first();
        if($employee){
            $department->employee_name = $employee->firstname . ' ' . $employee->lastname;
        }

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Viewed ' . $department->name . ' Department',
            'module'=>'Departments'
        ]);

        return view('pages.createDepartment')->with('showDepartment', $department);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $department = Department::where('id', $id)->firstOrFail();
        $employee = Employee::where('employee_id', $department->supervisor)->first();
        $empDept = Department::where('department_id', $employee->department_id)->first();
        $department->supervisor_id = $employee->employee_id;
        $department->supervisor_firstname = $employee->firstname;
        $department->supervisor_lastname = $employee->lastname;
        return view('pages.createDepartment')->with('updateDepartment', $department);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        if($request->input('type') == 'activate' || $request->input('type') == 'deactivate'){
            switch($request->input('type')){
                case 'activate':
                $status = 'active';
                break;
                case 'deactivate':
                $status = 'inactive';
                break;
            }
            $dept = Department::where('id', $id)->firstOrFail();
            $dept->status = $status;
            $dept->save();

            Audit::create([
                'employee_id'=>session('employee_id'),
                'action'=>$request->input('type') . 'd '. $dept->name . ' Department',
                'module'=>'Update Department'
            ]);

            return 'success';
        }

        $this->validate($request, [
            'deptName'=>'required',
            'supervisor'=>'required|exists:employee_details,employee_id'
        ]);

        $dept = Department::where('id', $id)->firstOrFail();
        $dept->name = $request->input('deptName');
        $dept->supervisor = $request->input('supervisor');
        $dept->save();

        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Updated ' . $request->input('deptName') . ' Department',
            'module'=>'Update Department'
        ]);

        return view('pages.departments')->with('departmentUpdated', $dept->name);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
    //
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function autocomplete(Request $request)
    {
        $term = trim($request->term);

        if (empty($term)) {
            $data = Department::where('status', '!=', 'inactive')->select('name', 'department_id')->get();
        }else{
            $data = Department::where('status', '!=', 'inactive')->select('name', 'department_id')
            ->Where('name', 'LIKE', '%'. $term .'%')->get();
        }

        $formatted_tags = [];

        foreach ($data as $tag) {
            $formatted_tags[] = ['id' => $tag->department_id, 'text' => $tag->name];
        }
        return response()->json($formatted_tags);
    }

    public function test(){
        dd(Department::select('department_id')->get());
        $access = AccessMatrix::join('tjsg_hris.employee_details', 'access_matrix1.employee_id', 'tjsg_hris.employee_details.employee_id')
        ->join('system_details', 'access_matrix1.system_id', 'system_details.id')
        ->select(DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE employee_id = access_matrix1.employee_id) AS full_name'), 'system_details.name', 'access_matrix1.hasAccess', 'access_matrix1.id')->get();
        $systemCount = DB::table('ltxx_systemadmin.system_details')->count();
        $access->systemCount = $systemCount;
        $access->empCount = AccessMatrix::distinct()->select('employee_id')->get()->count();
        $access->empList = AccessMatrix::distinct()->select('access_matrix1.employee_id', DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM tjsg_hris.employee_details WHERE employee_id = access_matrix1.employee_id) AS name'))->join('tjsg_hris.employee_details', 'access_matrix1.employee_id', 'tjsg_hris.employee_details.employee_id')->get();

        return view('pages.auditTrail')->with('test', $access);
    }
}
