<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Audit;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.changePassword');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'oldPassword'=>'required',
            'newPassword'=>'required|confirmed|min:6|different:oldPassword',
            'newPassword_confirmation'=>'required'
        ]);

        $user = User::where('employee_id', $id)->first();
        if(Auth::attempt(['email'=>$user->email, 'password' => $request->input('oldPassword')])){
            $user->password = Hash::make($request->input('newPassword'));
            $user->save();

            Audit::create([
                'employee_id'=>$id  ,
                'action'=>'Changed Password',
                'module'=>'Employee Profile'
            ]);

            return view('pages.changePassword')->with('passwordUpdated', true)->with('users', $user);
        }else{
            return view('pages.changePassword')->with('invalidPass', true)->with('users', $user);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
