<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\User;
use App\Department;
use App\Audit;
use App\Site;
use DB;
use DataTables;

class DatatablesController extends Controller
{
	public function employeeData(){
		$builder = Employee::query()->select('employee_details.id', 'employee_details.employee_id', DB::raw('(SELECT CONCAT(firstname," ", lastname) FROM employee_details ed WHERE id = employee_details.id) AS full_name'), 'employee_details.birth_date', 'employee_details.birth_place', 'employee_details.gender', 'employee_details.civil_status', 'employee_details.religion', 'employee_details.citizenship', 'employee_details.date_hired', 'employee_details.employee_status', 'employee_details.regularization_date', 'employee_details.separation_date', 'department_details.name', 'employee_details.position AS designation', 'employee_details.level', 'employee_details.site', 'compensation_benefits.tax_status', 'compensation_benefits.sss', 'compensation_benefits.tin', 'compensation_benefits.hdmf', 'compensation_benefits.phic', 'compensation_benefits.bpi', 'compensation_benefits.salary', 'employee_details.present_address', 'employee_details.mobile_number', 'employee_details.work_mobile_number', 'employee_details.cp_name', 'employee_details.cp_relationship', 'employee_details.cp_address', 'employee_details.cp_mobile', 'employee_details.school', 'employee_details.degree', 'employee_details.professional_license', 'employee_details.license_number', 'users.email')
		->leftJoin('ltxx_systemadmin.users', 'ltxx_systemadmin.users.employee_id', 'employee_details.employee_id')
		->leftJoin('compensation_benefits', 'employee_details.employee_id', 'compensation_benefits.employee_id')
		->leftJoin('department_details', 'employee_details.department_id', 'department_details.department_id')
    	->orderBy('date_hired', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
	}

	public function departmentData(){
		$builder = Department::query()->select('id', 'department_id', 'name', DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details WHERE employee_id = department_details.supervisor) AS supervisor'), 'status');
    	return datatables()->eloquent($builder)->toJson();
	}

	public function auditTrailData(){
		$builder = Audit::query()->select('id', 'employee_id', DB::raw('(SELECT CONCAT(firstname, " ", lastname) FROM employee_details ed WHERE employee_id = audit_trail.employee_id) AS full_name'), 'action', 'module', 'created_date')->orderBy('created_date', 'DESC');
    	return datatables()->eloquent($builder)->toJson();
	}

	public function siteData(){
		$builder = Site::query()->select('id', 'site_code', 'site_name');
    	return datatables()->eloquent($builder)->toJson();
	}
}
