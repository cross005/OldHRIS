<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Department;
use App\Audit;
use App\User;
use App\Benefit;
use File;

class FileController extends Controller
{
	public function importFileIntoDB(Request $request){
		$this->validate($request, [
			'employeeRecords'=>'required|file|mimes:xlsx,xls,csv'
		]);
		$path = $request->file('employeeRecords')->getRealPath();
		$data = \Excel::load($path)->get();
		if($data->count()){
			$i = 3;
			$j = 0;
			$x = array();
			$e[] = [];
			foreach ($data as $key => $value) {
				$ctr = 0;
				$arr = [];
				$arr2 = [];
				$arr3 = [];
				if($value->id_number == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must not be empty'];
				}

				if(!is_numeric($value->id_number)){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must only contain numbers.'];
				}

				$e = Employee::where('employee_id', $value->id_number)->first();
				if($e){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID already exists. [Owner: '. $e->firstname . ' '. $e->lastname .']'];
				}

				if(strlen($value->id_number) > 11){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must not be more than 11 characters.'];
				}

				if($value->birth_date == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Birth Date must not be empty.'];
				}

				if($value->first_name == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'First Name must not be empty'];
				}

				if(strlen($value->first_name) > 20){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'First Name must not be more than 20 characters'];
				}

				if(strlen($value->middle_name) > 20){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Middle Name must not be more than 10 characters'];
				}

				if($value->last_name == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Last Name must not be empty'];
				}

				if(strlen($value->last_name) > 30){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Last Name must not be more than 30 characters'];
				}
				if(!filter_var($value->company_e_mail, FILTER_VALIDATE_EMAIL)){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Company Email is not a valid email address'];
				}

				if($value->company_e_mail == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Company Email must not be empty'];
				}

				$e = User::where('email', $value->company_email)->first();
				if($e){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Company Email already exists'];
				}

				if($value->gender == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Gender must not be empty'];
				}

				if(strtolower($value->gender) !=  'male' && strtolower($value->gender) !=  'female'){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Gender must either be Male or Female'];
				}

				if($value->civil_status == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Civil Status must not be empty'];
				}

				if($value->date_hired == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Date Hired must not be empty'];
				}

				if($value->department == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Department must not be empty'];
				}
				$deptCode = Department::select('department_id')->where('name', $value->department)->first();
				if(!$deptCode){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>$value->department . ' does not exist in departments table'];
				}

				if(date('Y-m-d', strtotime($value->date_hired)) == $value->date_hired){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Date format must be YYYY-MM-DD'];
				}

				if($value->designation == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Position must not be empty'];
				}

				if(strlen($value->position) > 30){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Position must not be more than 30 characters'];
				}

				$pos = array('Probationary', 'Regular', 'Re-Hire', 'Consultant', 'Project Based', 'Intern', 'Resigned');

				if(!in_array($value->employee_status, $pos)){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Invalid status'];
				}

				if($value->employee_status == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employment Status must not be empty'];
				}

				if($value->site == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Site must not be empty'];
				}

				if(!is_numeric($value->salary)){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Salary must only contain numbers.'];
				}

				$i++;
				if($ctr < 1){
					$arr = [
						'employee_id'=>$value->id_number, 
						'firstname'=>$value->first_name, 
						'level'=>$value->level, 
						'name_extension'=>$value->name_extension, 
						'middlename'=>$value->middle_name, 
						'lastname'=>$value->last_name, 
						'birth_place'=>$value->birth_place, 
						'birth_date'=>date('Y-m-d', strtotime($value->birth_date)), 
						'gender'=>$value->gender, 
						'civil_status'=>$value->civil_status, 
						'religion'=>$value->religion, 
						'citizenship'=>$value->citizenship, 
						'department_id'=>isset($deptCode->department_id) ? $deptCode->department_id : $value->department , 
						'date_hired'=>date('Y-m-d', strtotime($value->date_hired)), 
						'position'=>$value->designation, 
						'employee_status'=>$value->employee_status, 
						'regularization_date'=>date('Y-m-d', strtotime($value->regularization_date)), 
						'separation_date'=>date('Y-m-d', strtotime($value->separation_date)), 
						'site'=>$value->site, 
						'present_address'=>$value->present_address, 
						'mobile_number'=>$value->mobile_number, 
						'personal_email'=>$value->personal_email
					];

					$arr2 = ['employee_id'=>$value->id_number,'tax_status'=>$value->tax_status, 'sss'=>$value->sss, 'tin'=>$value->tin, 'hdmf'=>$value->hdmf, 'phic'=>$value->phic, 'bpi'=>$value->bpi_account_number, 'salary'=>$value->salary];

					$arr3 = ['employee_id'=>$value->id_number, 'email'=>$value->company_e_mail];
					
					Employee::create($arr);
					Benefit::create($arr2);
					User::create($arr3);
				}else{
					array_push($x, [
						'ID Number'=>$value->id_number,
						'Last Name'=>$value->last_name,
						'First Name'=>$value->first_name,
						'Middle Name'=>$value->middle_name,
						'Name Extension'=>$value->name_extension,
						'Birth Date'=>$value->birth_date,
						'Birth Place'=>$value->birth_place,
						'Gender'=>$value->gender,
						'Civil Status'=>$value->civil_status,
						'Religion'=>$value->religion,
						'Citizenship'=>$value->citizenship,
						'Date Hired'=>$value->date_hired,
						'Employee Status'=>$value->employee_status,
						'Regularizatin Date'=>$value->regularization_date,
						'Separation Date'=>$value->separation_date,
						'Department'=>$value->department,
						'Designation'=>$value->designation,
						'Site'=>$value->site,
						'Tax Status'=>$value->tax_status,
						'SSS'=>$value->sss,
						'TIN'=>$value->tin,
						'HDMF'=>$value->hdmf,
						'PHIC'=>$value->phic,
						'BPI Account Number'=>$value->bpi_account_number,
						'Salary'=>$value->salary,
						'Company E-Mail'=>$value->company_e_mail,
						'level'=>$value->level,
						'Present Address'=>$value->present_address,
						'Personal'=>$value->mobile_number,
						'Personal Email'=>$value->personal_email,
						'Name'=>$value->name,
						'Relationship'=>$value->relationship,
						'Address'=>$value->address,
						'Contact Number'=>$value->contact_number,
						'School'=>$value->school,
						'Degree'=>$value->degree,
						'Professional License'=>$value->professional_license,
						'License No.'=>$value->license_number
					]);
				}
			}

			$filename = md5(rand(1,9999));
			if(!empty($err)){
				\Excel::create($filename, function($excel) use ($x) {
					$excel->sheet('sheet name', function($sheet) use ($x)
					{
						$sheet->fromArray($x, null, 'A2', false, true);
					});
				})->store('xlsx', storage_path('excel/exports'));
				$err[0]['file_name'] = $filename;
				foreach($err as $e){
					Audit::create([
					    'employee_id'=>session('employee_id'),
					    'action'=>'Row: ' . $e['row'] . ' - ' . $e['error_message'],
					    'module'=>'Import Error'
					]);
				}
				return view('pages.employees')->with('fieldErrors', $err);
			}else{
				Audit::create([
				    'employee_id'=>session('employee_id'),
				    'action'=>'Import Success without error. Row Total: ' . count($arr),
				    'module'=>'Import Employee'
				]);
				return view('pages.employees')->with('allEmployeeSuccess', true);
			}
		}
	} 

	public function batchUpdate(Request $request){
		$this->validate($request, [
			'employeeRecords'=>'required|file|mimes:xlsx,xls,csv'
		]);
		$path = $request->file('employeeRecords')->getRealPath();
		$data = \Excel::load($path)->get();

		if($data->count()){
			$i = 3;
			$j = 0;
			$x = array();
			$e[] = [];
			foreach ($data as $key => $value) {
				$ctr = 0;
				$arr = [];
				$arr2 = [];
				$arr3 = [];
				if($value->id_number == null){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must not be empty'];
				}

				if(!is_numeric($value->id_number)){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must only contain numbers.'];
				}

				$e = Employee::where('employee_id', $value->id_number)->first();
				if(!$e){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee does not exist. [ID: ' .$value->id_number.']'];
				}

				if(strlen($value->id_number) > 11){
					$j++;
					$ctr++;
					$err[] = ['error_no'=>$j, 'row'=>$i, 'error_message'=>'Employee ID must not be more than 11 characters.'];
				}

				$i++;
				if($ctr < 1){
					$arr = [
						'employee_id'=>$value->id_number, 
						'firstname'=>$value->first_name, 
						'level'=>$value->level, 
						'name_extension'=>$value->name_extension, 
						'middlename'=>$value->middle_name, 
						'lastname'=>$value->last_name, 
						'birth_place'=>$value->birth_place, 
						'birth_date'=>date('Y-m-d', strtotime($value->birth_date)), 
						'gender'=>$value->gender, 
						'civil_status'=>$value->civil_status, 
						'religion'=>$value->religion, 
						'citizenship'=>$value->citizenship, 
						'department_id'=>isset($deptCode->department_id) ? $deptCode->department_id : $value->department , 
						'date_hired'=>date('Y-m-d', strtotime($value->date_hired)), 
						'position'=>$value->designation, 
						'employee_status'=>$value->employee_status, 
						'regularization_date'=>date('Y-m-d', strtotime($value->regularization_date)), 
						'separation_date'=>date('Y-m-d', strtotime($value->separation_date)), 
						'site'=>$value->site, 
						'present_address'=>$value->present_address, 
						'mobile_number'=>$value->mobile_number, 
						'personal_email'=>$value->personal_email
					];

					$arr2 = ['employee_id'=>$value->id_number,'tax_status'=>$value->tax_status, 'sss'=>$value->sss, 'tin'=>$value->tin, 'hdmf'=>$value->hdmf, 'phic'=>$value->phic, 'bpi'=>$value->bpi_account_number, 'salary'=>$value->salary];

					$arr3 = ['employee_id'=>$value->id_number, 'email'=>$value->company_e_mail];
					
					Employee::where('employee_id', $value->id_number)->update(array_filter($arr));
					Benefit::where('employee_id', $value->id_number)->update(array_filter($arr2));
					User::where('employee_id', $value->id_number)->update(array_filter($arr3));
				}else{
					array_push($x, [
						'ID Number'=>$value->id_number,
						'Last Name'=>$value->last_name,
						'First Name'=>$value->first_name,
						'Middle Name'=>$value->middle_name,
						'Name Extension'=>$value->name_extension,
						'Birth Date'=>$value->birth_date,
						'Birth Place'=>$value->birth_place,
						'Gender'=>$value->gender,
						'Civil Status'=>$value->civil_status,
						'Religion'=>$value->religion,
						'Citizenship'=>$value->citizenship,
						'Date Hired'=>$value->date_hired,
						'Employee Status'=>$value->employee_status,
						'Regularizatin Date'=>$value->regularization_date,
						'Separation Date'=>$value->separation_date,
						'Department'=>$value->department,
						'Designation'=>$value->designation,
						'Site'=>$value->site,
						'Tax Status'=>$value->tax_status,
						'SSS'=>$value->sss,
						'TIN'=>$value->tin,
						'HDMF'=>$value->hdmf,
						'PHIC'=>$value->phic,
						'BPI Account Number'=>$value->bpi_account_number,
						'Salary'=>$value->salary,
						'Company E-Mail'=>$value->company_e_mail,
						'level'=>$value->level,
						'Present Address'=>$value->present_address,
						'Personal'=>$value->mobile_number,
						'Personal Email'=>$value->personal_email,
						'Name'=>$value->name,
						'Relationship'=>$value->relationship,
						'Address'=>$value->address,
						'Contact Number'=>$value->contact_number,
						'School'=>$value->school,
						'Degree'=>$value->degree,
						'Professional License'=>$value->professional_license,
						'License No.'=>$value->license_number
					]);
				}
			}

			$filename = md5(rand(1,9999));
			if(!empty($err)){
				\Excel::create($filename, function($excel) use ($x) {
					$excel->sheet('sheet name', function($sheet) use ($x)
					{
						$sheet->fromArray($x, null, 'A2', false, true);
					});
				})->store('xlsx', storage_path('excel/exports'));
				$err[0]['file_name'] = $filename;
				foreach($err as $e){
					Audit::create([
					    'employee_id'=>session('employee_id'),
					    'action'=>'Row: ' . $e['row'] . ' - ' . $e['error_message'],
					    'module'=>'Import Error'
					]);
				}
				return view('pages.employees')->with('fieldErrors', $err);
			}else{
				Audit::create([
				    'employee_id'=>session('employee_id'),
				    'action'=>'Import Success without error. Row Total: ' . count($arr),
				    'module'=>'Import Employee'
				]);
				return view('pages.employees')->with('allEmployeeUpdateSuccess', true);
			}
		}
	}

	public function downloadExcelFile($filename){
		$path = storage_path("excel\\exports\\" . $filename);
		if (!File::exists($path)) {
		    abort(404);
		}
		
		$file = File::get($path);
		$type = File::mimeType($path);

		$response = \Response::make($file, 200);
		$response->header("Content-Type", $type);
		Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Exported error sheet',
            'module'=>'Import Employee'
        ]);
		return $response;
	}      

	public function validateDate($date)
	{
	    $d = \DateTime::createFromFormat('Y-m-d', $date);
	    return $d && $d->format('Y-m-d') == $date;
	}
}

