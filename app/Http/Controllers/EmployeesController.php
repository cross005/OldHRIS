<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Employee;
use App\Department;
use App\User;
use App\Audit;
use App\Benefit;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employees');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.createEmployee');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'employee_id'=>'required|numeric|unique:employee_details,employee_id|digits_between:1,11',
            'birth_date'=>'required|date',
            'firstname'=>'required|max:20',
            'middlename'=>'max:10',
            'lastname'=>'required|max:30',
            'email'=>'required|email|unique:mysql2.users,email',
            'gender'=>'required|in:male,female',
            'civilStatus'=>'required',
            'date_hired'=>'required|date',
            'department'=>'required|exists:department_details,department_id',
            'position'=>'required|max:30',
            'salary'=>'numeric|nullable',
            'level'=>'required|in:Associate,Senior Associate,Team Lead,Officer,Manager,Head',
            'employee_status'=>'required|in:Probationary,Regular,Re-hire,Consultant,Project Based,Intern,Resigned',
            'site'=>'required'
        ]);

        $deptId = Department::where('department_id', $request->input('department'))->first();

        $employee = Employee::create([
            'employee_id'=>$request->input('employee_id'),
            'birth_date'=>date('Y-m-d', strtotime($request->input('birth_date'))),
            'firstname'=>$request->input('firstname'),
            'name_extension'=>$request->input('nameExtension'),
            'middlename'=>$request->input('middlename'),
            'lastname'=>$request->input('lastname'),
            'birth_place'=>$request->input('birth_place'),
            'gender'=>strtolower($request->input('gender')),
            'civil_status'=>$request->input('civilStatus'),
            'religion'=>$request->input('religion'),
            'citizenship'=>$request->input('citizenship'),
            'date_hired'=>date('Y-m-d', strtotime($request->input('date_hired'))),
            'department_id'=>$deptId->department_id,
            'position'=>$request->input('position'),
            'level'=>$request->input('level'),
            'employee_status'=>$request->input('employee_status'),
            'regularization_date'=>$request->input('regularization_date'),
            'separation_date'=>$request->input('separation_date'),
            'site'=>$request->input('site'),
            'present_address'=>$request->input('present_address'),
            'mobile_number'=>$request->input('mobile_number'),
            'personal_email'=>$request->input('personal_email'),
            'cp_name'=>$request->input('cp_name'),
            'cp_relationship'=>$request->input('cp_relationship'),
            'cp_address'=>$request->input('cp_address'),
            'cp_mobile'=>$request->input('cp_mobile'),
            'school'=>$request->input('school'),
            'degree'=>$request->input('degree'),
            'professional_license'=>$request->input('professional_license'),
            'license_number'=>$request->input('license_number')
        ]);

        $benefits = Benefit::create([
            'employee_id'=>$employee->employee_id,
            'tax_status'=>$request->input('tax_status'),
            'sss'=>$request->input('sss'),
            'tin'=>$request->input('tin'),
            'hdmf'=>$request->input('hdmf'),
            'phic'=>$request->input('phic'),
            'bpi'=>$request->input('bpi'),
            'salary'=>$request->input('salary')
        ]);

        $account = User::create([
            'employee_id'=>$employee->employee_id,
            'email'=>$request->input('email'),
            'status'=>'Active',
            'isDeleted'=>0
        ]);

        Audit::create([
            'employee_id'=>session('employee_id')  ,
            'action'=>'Created ' . $employee->firstname . '\'s profile',
            'module'=>'Employee Profile'
        ]);

        return view('pages.createEmployee')->with('employeeCreated', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::where('id', $id)->firstOrFail();
        $compBen = Benefit::where('employee_id', $employee->employee_id)->first();
        $dept = Department::where('department_id', $employee->department_id)->first();
        $employee->deptName = $dept->name;
        if($compBen){
            $employee->sss = $compBen->sss;
            $employee->tin = $compBen->tin;
            $employee->hdmf = $compBen->hdmf;
            $employee->phic = $compBen->phic;
            $employee->bpi = $compBen->bpi;
            $employee->salary = $compBen->salary;
        }
        $e = User::where('employee_id', $employee->employee_id)->first();
        $employee->company_email = $e->email;
        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Viewed ' . $employee->firstname . '\'s profile',
            'module'=>'Employee Profile'
        ]);
        return view('pages.createEmployee')->with('showEmployee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::where('id', $id)->firstOrFail();
        $compBen = Benefit::where('employee_id', $employee->employee_id)->first();
        $dept = Department::where('department_id', $employee->department_id)->first();
        $employee->deptName = $dept->name;
        if($compBen){
            $employee->tax_status = $compBen->tax_status;
            $employee->sss = $compBen->sss;
            $employee->tin = $compBen->tin;
            $employee->hdmf = $compBen->hdmf;
            $employee->phic = $compBen->phic;
            $employee->bpi = $compBen->bpi;
            $employee->salary = $compBen->salary;
        }
        $e = User::where('employee_id', $employee->employee_id)->first();
        $employee->company_email = $e->email;
        return view('pages.createEmployee')->with('updateEmployee', $employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $e = User::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'employee_id'=>[
                'required', 'numeric',
                Rule::unique('employee_details')->ignore($id),
            ],
            'birth_date'=>'required|date',
            'firstname'=>'required|max:20',
            'middlename'=>'max:10',
            'lastname'=>'required|max:30',
            'email'=>[
                'required', 'email',
                Rule::unique('mysql2.users')->ignore($e->id),
            ],
            'gender'=>'required|in:male,female',
            'civilStatus'=>'required',
            'date_hired'=>'required|date',
            'department'=>'required|exists:department_details,department_id',
            'position'=>'required|max:30',
            'salary'=>'numeric|nullable',
            'level'=>'required|in:Associate,Senior Associate,Team Lead,Officer,Manager,Head',
            'employee_status'=>'required|in:Probationary,Regular,Re-hire,Consultant,Project Based,Intern,Resigned',
            'site'=>'required'
        ]);

        $employees = Employee::where('id', $id)->update([
            'employee_id'=>$request->input('employee_id'),
            'birth_date'=>date('Y-m-d', strtotime($request->input('birth_date'))),
            'firstname'=>$request->input('firstname'),
            'name_extension'=>$request->input('nameExtension'),
            'middlename'=>$request->input('middlename'),
            'lastname'=>$request->input('lastname'),
            'birth_place'=>$request->input('birth_place'),
            'gender'=>strtolower($request->input('gender')),
            'civil_status'=>$request->input('civilStatus'),
            'religion'=>$request->input('religion'),
            'citizenship'=>$request->input('citizenship'),
            'date_hired'=>date('Y-m-d', strtotime($request->input('date_hired'))),
            'department_id'=>$request->input('department'),
            'position'=>$request->input('position'),
            'level'=>$request->input('level'),
            'employee_status'=>$request->input('employee_status'),
            'regularization_date'=>$request->input('regularization_date'),
            'separation_date'=>$request->input('separation_date'),
            'site'=>$request->input('site'),
            'present_address'=>$request->input('present_address'),
            'mobile_number'=>$request->input('mobile_number'),
            'personal_email'=>$request->input('personal_email'),
            'cp_name'=>$request->input('cp_name'),
            'cp_relationship'=>$request->input('cp_relationship'),
            'cp_address'=>$request->input('cp_address'),
            'cp_mobile'=>$request->input('cp_mobile'),
            'school'=>$request->input('school'),
            'degree'=>$request->input('degree'),
            'professional_license'=>$request->input('professional_license'),
            'license_number'=>$request->input('license_number')
        ]);

        $compBen = Benefit::where('employee_id', $request->input('employee_id'))->first();

        if($compBen){
            $upComp = Benefit::where('id', $compBen->id)->update([
                'tax_status'=>$request->input('tax_status'),
                'sss'=>$request->input('sss'),
                'tin'=>$request->input('tin'),
                'hdmf'=>$request->input('hdmf'),
                'phic'=>$request->input('phic'),
                'bpi'=>$request->input('bpi'),
                'salary'=>$request->input('salary'),
            ]);
        }else{
            $benefits = Benefit::create([
                'employee_id'=>$request->input('employee_id'),
                'tax_status'=>$request->input('tax_status'),
                'sss'=>$request->input('sss'),
                'tin'=>$request->input('tin'),
                'hdmf'=>$request->input('hdmf'),
                'phic'=>$request->input('phic'),
                'bpi'=>$request->input('bpi'),
                'salary'=>$request->input('salary')
            ]);
        }

        $updateLogin = User::where('employee_id', $request->input('employee_id'))->update([
            'email'=>$request->input('email')
        ]);

        $emp = Employee::where('id', $id)->first();
        Audit::create([
            'employee_id'=>session('employee_id'),
            'action'=>'Updated ' . $emp->firstname . '\'s profile',
            'module'=>'Update Employee Profile'
        ]);

        return view('pages.employees')->with('employeeUpdated', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {   
        $term = trim($request->term);

        if (empty($term)) {
            $data = Employee::select('firstname', 'middlename', 'lastname', 'employee_id', 'department_id')->get();
        }else{
            $data = Employee::select('firstname', 'middlename', 'lastname', 'employee_id', 'department_id')
            ->where('firstname', 'LIKE', '%'. $term .'%')
            ->orWhere('middlename', 'LIKE', '%'. $term .'%')
            ->orWhere('lastname', 'LIKE', '%' . $term . '%')
            ->orWhere('employee_id', 'LIKE', '%'. $term .'%')->get();
        }

        $formatted_tags = [];

        foreach ($data as $tag) {
            $formatted_tags[] = ['id' => $tag->employee_id, 'text' => $tag->employee_id . ' ' .$tag->firstname . ' ' . $tag->lastname];
        }

        return response()->json($formatted_tags);
    }
}
