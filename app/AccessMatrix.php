<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessMatrix extends Model
{
    protected $table = 'access_matrix_department';
    public $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'department_id', 'system_id', 'hasAccess'
    ];

    protected $connection = 'mysql2';
}
