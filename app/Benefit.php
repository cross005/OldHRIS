<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $table = 'compensation_benefits';
    public $primaryKey = 'id';

    public $timestamps = false;

    public $fillable = ['employee_id', 'tax_status', 'sss', 'tin', 'hdmf', 'phic', 'bpi', 'salary'];
}
