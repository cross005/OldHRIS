<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = 'audit_trail';
    public $primaryKey = 'id';

    public $timestamps = false;

    public $fillable = ['employee_id','action', 'module'];
}
